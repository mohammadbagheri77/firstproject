var app =new Vue({
    el: '#app',
    vuetify: new Vuetify(),

    data () {
        return {
            //navigation-drawer
            drawer: false,
            group: null,

            //carousel
            items: [
                {
                  src: 'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
                },
                {
                  src: 'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
                },
                {
                  src: 'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
                },
                {
                  src: 'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
                },
              ],
                
              //card
              cards: [
                {
                  title: 'CREST WATCH',
                  sub:'test test test test test test test test test test test test test test test test test test ',
                  back_img:'https://cdn.vuetifyjs.com/images/carousel/squirrel.jpg',
                  img:'images/3.png'
                },
                {
                  title: 'RAZAK DELICIUS',
                  sub:'test test test test test test test test test test test test test test test test test test ',
                  back_img:'https://cdn.vuetifyjs.com/images/carousel/sky.jpg',
                  img:'images/2.png'
                },
                {
                  title: 'MC STARS',
                  sub:'test test test test test test test test test test test test test test test test test test ',
                  back_img:'https://cdn.vuetifyjs.com/images/carousel/bird.jpg',
                  img:'images/1.png'
                },
                {
                  title: 'LAXMI',
                  sub:'test test test test test test test test test test test test test test test test test test ',
                  back_img:'https://cdn.vuetifyjs.com/images/carousel/planet.jpg',
                  img:'images/4.png'
                },

              ],

              //clients
              clients:[
                {
                  img:'images/1.png'
                },
                {
                  img:'images/2.png'
                },
                {
                  img:'images/3.png'
                },
                {
                  img:'images/4.png'
                },
                {
                  img:'images/1.png'
                },
                
              ],
              //footer
                  icons: [
                    'mdi-facebook',
                    'mdi-twitter',
                    'mdi-linkedin',
                    'mdi-instagram',
                  ],
         
        }
      },


     //navigation-drawer
      watch: {
        group () {
          this.drawer = false
        },
      }, 

  })

